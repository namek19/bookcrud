package com.namek.bookapp.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "book")
@Builder
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(nullable = false)
    public String author;

    @Column(nullable = false)
    public String title;

    @Column(nullable = false)
    public String date;

    @Column(nullable = false)
    public String editor;


    @Override
    public String toString() {
        return String.format("Book: %n - %s, %n - %s %n - %s, %n - %s ",
                this.author, this.title, this.date, this.editor);
    }

}
