package com.namek.bookapp.entity;

import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public record ErrorEntity(
        LocalDateTime timeStamp,
        String message,
        String errorAuthor,
        int httpStatus
) {
}
