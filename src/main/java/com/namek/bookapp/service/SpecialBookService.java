package com.namek.bookapp.service;

public interface SpecialBookService extends BookService{
    void openBookToPage(int page);
}
