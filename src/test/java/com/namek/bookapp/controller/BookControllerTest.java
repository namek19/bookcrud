package com.namek.bookapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.namek.bookapp.entity.Book;
import com.namek.bookapp.service.BookServiceImp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(BookControllerTest.class)
class BookControllerTest {

    private MockMvc mockMvc;
    @MockBean
    BookServiceImp bookService;

    @InjectMocks
    private BookController bookController;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(bookController).build();
    }


    @Test
    void save() throws Exception {
        // TODO Arrange

        // TODO act

        //TODO assert

    }

    @Test
    void getBookById() {
    }

    @Test
    void getBook() {
    }

    @Test
    void deleteBook() {
    }
}